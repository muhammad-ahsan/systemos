/*
 * Programmed by Muhammad Ahsan
 * muhammad.ahsan@gmail.com
 * Research Intern
 * Yahoo! Research Barcelona
 * Spain
 */
package systemos;

/**
 *
 * @author Ahsan
 */
public class SystemOS {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        /*
         * Get os.name system property using
         * public static String getProperty(String name) method of
         * System class.
         */

        String strOSName = System.getProperty("os.name");
        System.out.println(strOSName);
        System.out.println("Now Java version " + System.getProperty("sun.arch.data.model"));
    }
}
